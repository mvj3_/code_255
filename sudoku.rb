# simple sudoku solver
 
def has_duplicates? str
  r = str.reject{|i| i == '0'}
  r.size > r.uniq.size
end
 
def get_first_free all
  all.index('0')
end
 
def all_filled? all
  get_first_free(all).nil?
end
 
def get_row i,all
  all[i*9,9]
end
 
def get_col i,all
  (0..8).map{|row| all[row*9+i]}
end
 
def get_square i,all
  (0..2).map do |sr|
    all[(i/3*3 + sr)*9+i%3*3,3]
  end.flatten
end
 
def valid_field? pos, all
  return false if has_duplicates?(get_row(pos/9, all))
  return false if has_duplicates?(get_col(pos%9, all))
  return false if has_duplicates?(get_square(pos/9/3*3+pos%9/3, all))
  true
end
 
def check all
  pos = get_first_free all
  return all unless pos
                
  ('1'..'9').each do |i|
    all[pos] = i
    next unless valid_field? pos, all
    r = check all.dup
    return r if r
  end
  return false
end
 
all = ''
while line = gets
  all += line.chomp
end
all = all.split('')
r = check all
(0..8).each do |row|
  puts get_row(row, r).join('')
end